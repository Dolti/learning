package com.dolata.patryk;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int decimal = scanner.nextInt();
        double result = 0;
        int multiplier = 1;
        String resultString = "";

        while(decimal > 0)
        {
            int residue = decimal % 2;
            decimal     = decimal / 2;
            result      = result + residue * multiplier;
            resultString += residue+"";
            multiplier  = multiplier * 10;
        }
        int max = 1;
        int tmpMax = 1;
        char tmp = '0';
        for(int i =resultString.length()-1; i >= 0 ; i--){
            if(tmp == '1' && resultString.charAt(i) == '1'){
                max++;
            }else{
                if(max >= tmpMax){
                    tmpMax = max;
                }
                max = 1;

            }

            tmp = resultString.charAt(i);
        }
        if(tmpMax > max){
            System.out.println(tmpMax);
        } else {
            System.out.println(max);
        }
    }
}
