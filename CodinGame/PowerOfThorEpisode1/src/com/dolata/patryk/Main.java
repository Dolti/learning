package com.dolata.patryk;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int lightX = in.nextInt(); // the X position of the light of power
        int lightY = in.nextInt(); // the Y position of the light of power
        int initialTX = in.nextInt(); // Thor's starting X position
        int initialTY = in.nextInt(); // Thor's starting Y position
        int distaceX = lightX - initialTX;
        int distanceY = lightY - initialTY;
        int TX = initialTX;
        int TY = initialTY;
        // game loop
       // while (true) {
            int remainingTurns = in.nextInt(); // The remaining amount of turns Thor can move. Do not remove this line.
            distaceX = lightX - initialTX;
            distanceY = lightY - initialTY;
            System.err.println(distaceX);
            System.err.println(distanceY);

            // A single line providing the move to be made: N NE E SE S SW W or NW
            //int[][] moves = new int[8][2];

            Map<String, Point> moves = new HashMap<>();
            moves.put("N", calculatePosition("N", TX, TY));
            moves.put("NE", calculatePosition("NE", TX, TY));
            moves.put("E", calculatePosition("E", TX, TY));
            moves.put("SE", calculatePosition("SE", TX, TY));
            moves.put("S", calculatePosition("S", TX, TY));
            moves.put("SW", calculatePosition("SW", TX, TY));
            moves.put("W", calculatePosition("W", TX, TY));
            moves.put("NW", calculatePosition("NW", TX, TY));

        //}
    }

    public static Point calculatePosition(String direction, int currPosX, int currPosY ){
        Point point = new Point();
        switch (direction){
            case "N":
                point.x = currPosX;
                point.y = currPosY - 1;
                break;
            case "NE":
                point.x = currPosX + 1;
                point.y = currPosY - 1;
                break;
            case "E":
                point.x = currPosX + 1;
                point.y = currPosY;
                break;
            case "SE":
                point.x = currPosX + 1;
                point.y = currPosY + 1;
                break;
            case "S":
                point.x = currPosX;
                point.y = currPosY + 1;
                break;
            case "SW":
                point.x = currPosX - 1;
                point.y = currPosY + 1;
                break;
            case "W":
                point.x = currPosX - 1;
                point.y = currPosY;
                break;
            case "NW":
                point.x = currPosX - 1;
                point.y = currPosY - 1;
                break;
        }
        return point;
    }
}
