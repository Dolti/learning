package com.dolti;

/*
Everyone knows passphrases. One can choose passphrases from poems, songs, movies names and so on but frequently they can be guessed due to common cultural references. You can get your passphrases stronger by different means. One is the following:

choose a text in capital letters including or not digits and non alphabetic characters,

shift each letter by a given number but the transformed letter must be a letter (circular shift),
replace each digit by its complement to 9,
keep such as non alphabetic and non digit characters,
downcase each letter in odd position, upcase each letter in even position (the first character is in position 0),
reverse the whole result.
Example:

your text: "BORN IN 2015!", shift 1
1 + 2 + 3 -> "CPSO JO 7984!"
4 "CpSo jO 7984!"
5 "!4897 Oj oSpC"

With longer passphrases it's better to have a small and easy program. Would you write it?
 */
public class Main {

    public static void main(String[] args) {
        String test = "MY GRANMA CAME FROM NY ON THE 23RD OF APRIL 2015";
        System.out.println(playPass(test, 2));
    }

    public static String playPass(String s, int n) {
        StringBuilder result = new StringBuilder(s);
        for (int i = 0; i < s.length(); i++) {
            if (Character.isDigit(s.charAt(i))) {
                result.setCharAt(i, (char) ('0' + (9 - Integer.parseInt("" + s.charAt(i)))));
            } else if (Character.isLetter(s.charAt(i))) {
                char c = (char) (s.charAt(i) + n);
                if (c > 90) {
                    int letter = (int) c;
                    c = (char) (64 + (letter - 90));
                }
                if (i % 2 == 0) {
                    result.setCharAt(i, Character.toUpperCase(c));
                } else {
                    result.setCharAt(i, Character.toLowerCase(c));
                }
            } else {
                result.setCharAt(i, s.charAt(i));
            }
        }
        return result.reverse().toString();
    }
}
