package com.dolata.patryk;

/*
In this Kata, you need to simulate an old mobile display, similar to this one:

***************************
*                         *
*                         *
*        CodeWars         *
*                         *
*                         *
* Menu           Contacts *
***************************
Input Parameters:

number of characters for width (n)
height-to-width ratio in percentage (p)
Example: if n=30 and p=40, then display will be 30 characters large and its height will be 40% of n.

Rules and Notes:

the border, as you can see, is filled with *;
the rounding of divisions and float numbers is always by the integer (1.2, 1.5, 1.9 they are always reduced to 1), keep this in mind when you calculate proportions;
the menus Menu and Contacts are always in the second last line, at 1 character distance respectively from the left and from the right border;
the CodeWars logo is always in the middle horizontally and in the half-1 line vertically;
the width n must be always at least 20 characters and the percentage p must be always at least 30%, take care of this (otherwise menus won't likely fit).
random tests might get big and percentages might be higher than 100;
OFF-TOPIC before Examples:

A bit of advertisement for an old unnoticed Kata that I translated from Python to Javascript: Character Counter and Bars Graph

Examples:

mobileDisplay(30,40):
******************************
*                            *
*                            *
*                            *
*                            *
*          CodeWars          *
*                            *
*                            *
*                            *
*                            *
* Menu              Contacts *
******************************
mobileDisplay(25,50):
*************************
*                       *
*                       *
*                       *
*                       *
*       CodeWars        *
*                       *
*                       *
*                       *
*                       *
* Menu         Contacts *
*************************

 */
public class Main {

    public static void main(String[] args) {
        String solution = "***************************\n*                         *\n*                         *\n*                         *\n*        CodeWars         *\n*                         *\n*                         *\n*                         *\n*                         *\n* Menu           Contacts *\n***************************";
        String sol2 = "********************\n" +
                "*                  *\n" +
                "*                  *\n" +
                "*                  *\n" +
                "*                  *\n" +
                "*                  *\n" +
                "*     CodeWars     *\n" +
                "*                  *\n" +
                "*                  *\n" +
                "*                  *\n" +
                "*                  *\n" +
                "*                  *\n" +
                "* Menu    Contacts *\n" +
                "********************";
        System.out.println(mobileDisplay(63, 17));
        System.out.println(solution);
        compare(sol2, mobileDisplay(20, 70));

    }

    public static void compare (String s1, String s2){
        if(s1.length() != s2.length()){
            System.out.println("Rozne dlugosci");
            if(s1.length() > s2.length()){
                for(int i = 0; i < s1.length(); i++){
                    System.out.print(s1.charAt(i));
                    if(s1.charAt(i) != s2.charAt(i)){
                        System.out.print("!");
                        break;
                    }
                }
            }else if(s2.length() > s1.length())
            {
                for(int i = 0; i < s2.length(); i++){
                    System.out.print(s2.charAt(i));
                    if(s1.charAt(i) != s2.charAt(i)){
                        System.out.print("!");
                        break;
                    }
                }
            }
        } else {
            System.out.println("Takie same dlugosci");
            System.out.println("Czy equals = " + s1.equals(s2));
        }

    }

    public static String mobileDisplay(int n, int p) {
        if (n < 20 ){
            n = 20;
        }
        if(p < 30){
            p = 30;
        }
        String result = "";
        int linesAboveLogo = countLinesAboveLogo(n, (float)p/100);
        int linesBelowLogo = countLinesBelowLogo(n, (float)p/100);

        result = topBottomLine(n);
        result += "\n";
        result += spaceAboveBelowLogo(n,linesAboveLogo);
        result += logoLine(n);
        result += spaceAboveBelowLogo(n, linesBelowLogo);
        result += menuLine(n);
        result += topBottomLine(n);
        return result;
    }

    public static String topBottomLine(int n) {
        String result = "";
        for (int i = 0; i < n; i++) {
            result += "*";
        }
        return result;
    }

    public static int countLinesAboveLogo(int n, float p){
        int result = 0;
        int sumOfLines = (int)(n * p) - 3;
        result = sumOfLines / 2;
        if(sumOfLines % 2 == 0 ){
            result -= 1;
        }
        return result;
    }

    public static int countLinesBelowLogo(int n, float p){
        int result = 0;
        int sumOfLines = (int)(n * p) - 3;
        result = sumOfLines / 2;
        return result;
    }

    public static String spaceAboveBelowLogo(int n, int l) {
        String result = "";
        String oneLine = "*";

        for (int i = 0; i < (n - 2); i++) {
            oneLine += " ";
        }
        oneLine += "*\n";

        for (int i = 0; i < l; i++) {
            result += oneLine;
        }
        return result;
    }

    public static String logoLine(int n) {
        String result = "*";
        for (int i = 0; i < (n - 10) / 2; i++) {
            result += " ";
        }
        result += "CodeWars";
        for (int i = 0; i < (n - 10) / 2; i++) {
            result += " ";
        }
        if (result.length() + 1 != n) {
            result += " *\n";
        } else {
            result += "*\n";
        }
        return result;
    }

    public static String menuLine(int n) {
        String result = "* Menu";
        for (int i = 0; i < n - 16; i++) {
            result += " ";
        }
        result += "Contacts *\n";
        return result;
    }
}
/* Best from codewars
public static String mobileDisplay(int n, int p) {
    n = Math.max(20, n);
    p = Math.max(30, p);
    final int h = (int)(n * p/100.);
    String s = "";
    for (int i = 0; i < h; i++) {
      String row = "";
      for (int j = 0; j < n; j++) {
        if (i == 0 || i == h-1 || j == 0 || j == n-1) { row += "*"; }
        else if (i == h/2-1 && j == n/2-4) { row += "CodeWars"; j += 7; }
        else if (i == h-2 && j == 2) { row += "Menu"; j += 3; }
        else if (i == h-2 && j == n-10) { row += "Contacts"; j += 7; }
        else { row += " "; }
      }
      row += i == h-1 ? "" : "\n";
      s += row;
    }
    return s;
  }
}
 */
