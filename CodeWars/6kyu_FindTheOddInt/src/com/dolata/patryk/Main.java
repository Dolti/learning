package com.dolata.patryk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        int[] A = {20,1,-1,2,-2,3,3,5,5,1,2,4,20,4,-1,-2,5};
        System.out.println(findIt(A));
    }

    public static int findIt(int[] A) {
        List<Integer> list = new ArrayList<>();
        for(int i = 0; i < A.length; i++) list.add(A[i]);
        for(int i = 0; i < A.length; i++) if(  Collections.frequency(list, A[i]) % 2 != 0) return A[i];
        return 0;
    }

/*
        public static int findIt(int[] A) {
            int xor = 0;
            for (int i = 0; i < A.length; i++) {
                xor ^= A[i];
            }
            return xor;
        }
*/
}
