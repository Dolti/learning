package com.dolti;

/*
* How it works

You notice that each flap character is on some kind of a rotor and the order of characters on each rotor is:

ABCDEFGHIJKLMNOPQRSTUVWXYZ ?!@#&()|<>.:=-+* /0123456789
And after a long while you deduce that the display works like this:
Starting from the left, all rotors (from the current one to the end of the line) flap together until the left-most rotor character is correct.
Then the mechanism advances by 1 rotor to the right...
...REPEAT this rotor procedure until the whole line is updated
...REPEAT this line procedure from top to bottom until the whole display is updated
Example
Consider a flap display with 3 rotors and one 1 line which currently spells CAT
Step 1 (current rotor is 1)
Flap x 1
Now line says DBU
Step 2 (current rotor is 2)
Flap x 13
Now line says DO)
Step 3 (current rotor is 3)
Flap x 27
Now line says DOG
lines  // array of strings. Each string is a display line of the initial configuration
rotors // array of array-of-rotor-values. Each array-of-rotor-values is applied to the corresponding display line
result // array of strings. Each string is a display line of the final configuration

lines = ["CAT"]
rotors = [[1,13,27]]
result = ["DOG"]

Kata Task
Given the initial display lines and the rotor moves for each line, determine what the board will say after it has been fully updated.
For your convenience the characters of each rotor are in the pre-loaded constant ALPHABET which is a string.

*/
public class Main {
    private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ?!@#&()|<>.:=-+*/0123456789";
    public static String[] flapDisplay(final String[] lines, final int[][] rotors) {
        int l = 0;
        String[] result = new String[lines.length];
        for (String line : lines) {
            String tmp = line;
            for (int i = 0; i < line.length(); i++) {
                tmp = rollLetters(tmp, rotors[l][i], i);
            }
            result[l] = tmp;
            System.out.println(tmp);
            l++;
        }
        return result;
    }

    public static String rollLetters(String line, int rotate, int position) {
        StringBuilder s = new StringBuilder(line);
        for (int i = position; i < s.length(); i++) {
            s.setCharAt(i, ALPHABET.charAt(getIndex(s.charAt(i), rotate)));
        }
        return s.toString();
    }

    public static int getIndex(char c, int rotate){
        int index = ALPHABET.indexOf(c);
        int times = (index + rotate) / ALPHABET.length();

        if (times > 0) {
            index = (index + rotate) - times*ALPHABET.length();
        } else {
            index += rotate;
        }
        return index;
    }
    public static void main(String[] args) {
        String[] lines = {"CAT", "HELLO ", "CODE"};
        int[][] rotors = {{1, 13, 27}, {15, 49, 50, 48, 43, 13}, {20,20,28,0}};
        System.out.println(flapDisplay(lines, rotors));
    }
}
