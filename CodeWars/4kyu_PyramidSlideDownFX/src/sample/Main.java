package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        //Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        GridPane root = new GridPane();
        //root.setAlignment(Pos.CENTER);
        root.setVgap(10);
        root.setHgap(10);
        root.setMinWidth(750);
        root.setMinHeight(375);
        Label greeting = new Label("0");
        greeting.setId("Result");
        greeting.setTextFill(Color.RED);
        greeting.setFont(Font.font("Times New Roman", FontWeight.BOLD, 70));
        root.getChildren().add(greeting);
        addButtons(root, greeting);

        primaryStage.setTitle("Pyramid Slide Down");
        primaryStage.setScene(new Scene(root, 750, 750));
        primaryStage.show();
    }

    public static void addButtons(GridPane root, Label result) {
        int[][] test = new int[][]{
                {75},
                {95, 64},
                {17, 47, 82},
                {18, 35, 87, 10},
                {20, 4, 82, 47, 65},
                {19, 1, 23, 75, 3, 34},
                {88, 2, 77, 73, 7, 63, 67},
                {99, 65, 4, 28, 6, 16, 70, 92},
                {41, 41, 26, 56, 83, 40, 80, 70, 33},
                {41, 48, 72, 33, 47, 32, 37, 16, 94, 29},
                {53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14},
                {70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57},
                {91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48},
                {63, 66, 4, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31},
                {4, 62, 98, 27, 23, 9, 70, 98, 73, 93, 38, 53, 60, 4, 23},
        };

//        int[][]test = new int[][]{
//                {3}, {7, 4}, {2, 4, 6}, {8, 5, 9, 3}
//        };
        int mask[][] = longestSlideDown(test);
        int posX = (test[test.length - 1].length - 1) * 25;
        for (int i = 0; i < test.length; i++) {
            HBox hBox = new HBox();
            hBox.setPadding(new Insets(0, 0, 0, posX));
            for (int j = 0; j < test[i].length; j++) {
                Button button = new Button(test[i][j] + "");
                button.setMinWidth(50);
                button.setMinHeight(25);
                button.setId("Button_" + i + j);
                if(mask[i][j] == 1){
                    button.setStyle("-fx-background-color: #ff0000;");
                    int sum = Integer.parseInt(result.getText()) + Integer.parseInt(button.getText());
                    result.setText(sum + "");
                }
                button.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        if(button.getStyle() == "-fx-background-color: #ff0000;"){
                            button.setStyle("");
                            int sum = Integer.parseInt(result.getText()) - Integer.parseInt(button.getText());
                            result.setText(sum + "");
                        } else {
                            button.setStyle("-fx-background-color: #ff0000;");
                            int sum = Integer.parseInt(result.getText()) + Integer.parseInt(button.getText());
                            result.setText(sum + "");
                        }
                    }
                });
                hBox.getChildren().add(button);
            }
            posX -= 25;
            root.getChildren().add(hBox);
            root.setRowIndex(hBox, i + 1);
        }
    }

    public static void main(String[] args) {
        int[][] test = new int[][]{
                {75},
                {95, 64},
                {17, 47, 82},
                {18, 35, 87, 10},
                {20, 4, 82, 47, 65},
                {19, 1, 23, 75, 3, 34},
                {88, 2, 77, 73, 7, 63, 67},
                {99, 65, 4, 28, 6, 16, 70, 92},
                {41, 41, 26, 56, 83, 40, 80, 70, 33},
                {41, 48, 72, 33, 47, 32, 37, 16, 94, 29},
                {53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14},
                {70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57},
                {91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48},
                {63, 66, 4, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31},
                {4, 62, 98, 27, 23, 9, 70, 98, 73, 93, 38, 53, 60, 4, 23},
        };
        showNicePyramid(test);
        System.out.println("Wynik = " + longestSlideDown(test));
        launch(args);
    }

    public static int[][] longestSlideDown(int[][] pyramid) {
        int result = pyramid[0][0];
        int lastIndex = 0;
        int road[] = new int[pyramid.length];
        int mask[][] = new int[pyramid.length][pyramid[pyramid.length-1].length];
        mask[0][0] = 1;
        road[0] = pyramid[0][0];
        int[] tab = new int[pyramid.length];
        for (int i = 0; i < pyramid.length; i++) {
            int right = getSum(result, i, lastIndex, "right", pyramid);
            int left = getSum(result, i, lastIndex, "left", pyramid);
            int tmpResultRight = 0;
            int tmpResultLeft = 0;
            int tmpResultMid = 0;
            if (i + 2 < pyramid.length && lastIndex + 2 < pyramid[pyramid.length - 1].length) {
                tmpResultRight = pyramid[i + 2][lastIndex + 2];
                tmpResultLeft = pyramid[i + 2][lastIndex];
                tmpResultMid = pyramid[i + 2][lastIndex+1];
            }
            if ((right + tmpResultRight > left + tmpResultLeft)&&(right + tmpResultRight >tmpResultMid + left )) {
                result += right;
                lastIndex = lastIndex + 1;
                road[i+1] = right;
                mask[i+1][lastIndex] = 1;
            } else if ((left + tmpResultLeft > tmpResultRight + right)&&(left + tmpResultRight >tmpResultMid + right )) {
                result += left;
                road[i+1] = left;
                mask[i+1][lastIndex] = 1;
            } else if (right > left) {
                result += right;
                lastIndex = lastIndex + 1;
                road[i+1] = right;
                mask[i+1][lastIndex] = 1;

            } else if (left > right) {
                result += left;
                road[i+1] = left;
                mask[i+1][lastIndex] = 1;
            }
        }
        System.out.println();
        for (int x = 0; x < road.length; x++) {
            System.out.print(road[x] + " ");
        }

        System.out.println();
        showNicePyramid(mask);
        return mask;
    }

    public static int getSum(int currentSum, int currentRow, int currentIndex, String direction, int[][] pyramid) {
        if (currentRow + 1 == pyramid.length || currentIndex + 1 == pyramid[currentIndex + 1].length) return 0;
        int sum = 0;
        switch (direction) {
            case "right":
                sum = pyramid[currentRow + 1][currentIndex + 1];
                break;
            case "left":
                sum = pyramid[currentRow + 1][currentIndex];
                break;
        }

        return sum;
    }

    public static void showNicePyramid(int[][] pyramid) {
        String lastRow = "";
        for (int i = 0; i < pyramid[pyramid.length - 1].length - 1; i++) {
            lastRow += pyramid[pyramid.length - 1][i] + " ";
        }
        lastRow += pyramid[pyramid.length - 1][pyramid[pyramid.length - 1].length - 1];
        int maxLength = lastRow.length();
        for (int i = 0; i < pyramid.length; i++) {
            String currentLine = "";
            for (int j = 0; j < pyramid[i].length - 1; j++) {
                currentLine += pyramid[i][j] + " ";
            }
            currentLine += pyramid[i][pyramid[i].length - 1];
            int delta = (maxLength - currentLine.length()) / 2;
            String result = "";
            for (int x = 0; x < delta; x++) {
                result += " ";
            }
            result += currentLine;
            for (int x = 0; x < delta; x++) {
                result += " ";
            }
            System.out.println(result);
        }
    }
}
