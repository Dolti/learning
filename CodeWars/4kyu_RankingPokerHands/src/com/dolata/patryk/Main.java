package com.dolata.patryk;

/*
A famous casino is suddenly faced with a sharp decline of their revenues. They decide to offer Texas hold'em also online. Can you help them by writing an algorithm that can rank poker hands?

Task:

Create a poker hand that has a method to compare itself to another poker hand:
    Result PokerHand.compareWith(PokerHand hand);
A poker hand has a constructor that accepts a string containing 5 cards:
    PokerHand hand = new PokerHand("KS 2H 5C JD TD");
The characteristics of the string of cards are:
A space is used as card seperator
Each card consists of two characters
The first character is the value of the card, valid characters are:
2, 3, 4, 5, 6, 7, 8, 9, T(en), J(ack), Q(ueen), K(ing), A(ce)
The second character represents the suit, valid characters are:
S(pades), H(earts), D(iamonds), C(lubs)

The result of your poker hand compare can be one of these 3 options:
    public enum Result
    {
        WIN,
        LOSS,
        TIE
    }
Apply the Texas Hold'em rules for ranking the cards.
There is no ranking for the suits.
 */
public class Main {

    public static void main(String[] args) {
        PokerHand pokerHand = new PokerHand("2H 3D 4H 5H 6H 2C KD");
        System.out.println(pokerHand.getRank());
//
//        System.out.println("Game has started:");
//        PokerGame pokerGame = new PokerGame();
//        Player pat = new Player("Patrick", 1000);
//        Player bob = new Player("Bob", 1000);
//        pat.setPokerHand(new PokerHand(pokerGame.getCards(2)));
//        bob.setPokerHand(new PokerHand(pokerGame.getCards(2)));
//
//        Player[] players = {pat, bob};
//        pokerGame.takeCoins(players);
//        pokerGame.showFlop();
//        pokerGame.takeCoins(players);
//        pokerGame.showTurn();
//        pokerGame.takeCoins(players);
//        pokerGame.showRiver();
//        pokerGame.takeCoins(players);
//        pokerGame.getWinner(players);
//
//        PokerHand pokerHand = new PokerHand(pokerGame.getCards(5));
//	    PokerHand pokerHand2 = new PokerHand(pokerGame.getCards(5));
//        System.out.println(pokerHand.compareWith(pokerHand2));
//         pokerHand = new PokerHand("2H 3H 4H 5H 6H");
//        System.out.println(pokerHand.getRank());
//         pokerHand = new PokerHand("AS AH 2H AD AC");
//        System.out.println(pokerHand.getRank());
//         pokerHand = new PokerHand("2S AH 2H AS AC");
//        System.out.println(pokerHand.getRank());
//         pokerHand = new PokerHand("2S AH 2H AS AC");
//        System.out.println(pokerHand.getRank());
//         pokerHand = new PokerHand("AS 3S 4S 8S 2S");
//        System.out.println(pokerHand.getRank());
//         pokerHand = new PokerHand("2H 3H 5H 6H 7H");
//        System.out.println(pokerHand.getRank());
//         pokerHand = new PokerHand("2S 3H 4H 5S 6C");
//        System.out.println(pokerHand.getRank());
//         pokerHand = new PokerHand("2S 3H 4H 5S 6C");
//        System.out.println(pokerHand.getRank());
//         pokerHand = new PokerHand("2S 2H 4H 5S 4C");
//        System.out.println(pokerHand.getRank());
//         pokerHand = new PokerHand("2S 2H 4H 5S 4C");
//        System.out.println(pokerHand.getRank());
//         pokerHand = new PokerHand("6S AD 7H 4S AS");
//        System.out.println(pokerHand.getRank());
//         pokerHand = new PokerHand("2S AH 4H 5S KC");
//        System.out.println(pokerHand.getRank());
//         pokerHand = new PokerHand("2S 3H 6H 7S 9C");
//        System.out.println(pokerHand.getRank());
//         pokerHand = new PokerHand("4S 5H 6H TS AC");
//        System.out.println(pokerHand.getRank());
    }


}
