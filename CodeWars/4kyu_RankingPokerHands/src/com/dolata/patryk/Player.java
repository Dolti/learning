package com.dolata.patryk;

/**
 * Created by Dolti on 2017-01-11.
 */
public class Player {
    private String name;
    private PokerHand pokerHand;
    private int coins;

    public Player(String name, int coins) {
        this.name = name;
        this.coins = coins;
    }

    public int bet(int coins){
        if( coins > this.coins)
        {
            System.out.println("You havent enough money. Your current coins: " + this.coins);
            return 0;
        }
        this.coins -= coins;
        return coins;
    }

    public String getName() {
        return name;
    }

    public PokerHand getPokerHand() {
        return pokerHand;
    }

    public void setPokerHand(PokerHand pokerHand) {
        this.pokerHand = pokerHand;
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }
}
