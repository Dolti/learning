package com.dolata.patryk;

/**
 * Created by Dolti on 2017-01-10.
 */
public class PokerHand {
    public enum Result {TIE, WIN, LOSS}
    public enum Ranks {HIGH_CARD, ONE_PAIR, TWO_PAIR, THREE_OF_KIND, STRAIGHT, FLUSH, FULL_HOUSE, FOUR_OF_KIND, POKER}

    private String hand;
    private Ranks rank = Ranks.HIGH_CARD;
    private String[] orders = {"2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A"};

    PokerHand(String hand) {
        this.hand = hand;
        rank = getRankFromCards(hand);
        System.out.println(rank);
    }
    public void addCards(String cards){
        this.hand += " " + cards;
        rank = getRankFromCards(hand);
        System.out.println(rank);
    }
    public Result compareWith(PokerHand opponent) {
        if (this.rank.ordinal() > opponent.rank.ordinal()) {
            return Result.WIN;
        } else if (this.rank.ordinal() < opponent.rank.ordinal()) {
            return Result.LOSS;
        } else {
            String[] cards = this.hand.split(" ");
            String[] opponentCards = opponent.hand.split(" ");
            sortCards(cards);
            sortCards(opponentCards);
            int index = 0;
            while (index < 5) {
                char highest = cards[index].charAt(0);
                char opponentHighest = opponentCards[index].charAt(0);
                if (findId(highest + "") > findId(opponentHighest + "")) {
                    return Result.WIN;
                } else if (findId(highest + "") < findId(opponentHighest + "")) {
                    return Result.LOSS;
                }
                index++;
            }
        }
        return Result.TIE;
    }

    public Ranks getRank() {
        return this.rank;
    }

    public void setRank(Ranks rank) {
        rank = rank;
    }

    public String getHand() {
        return this.hand;
    }

    public void setHand(String hand) {
        this.hand = hand;
    }

    private Ranks getRankFromCards(String hand) {
        Ranks rank = Ranks.HIGH_CARD;
        String[] cards = hand.split(" ");

        if (isPoker(cards)) {
            rank = Ranks.POKER;
        } else if (isFourOfaKind(cards)) {
            rank = Ranks.FOUR_OF_KIND;
        } else if (isFullHouse(cards)) {
            rank = Ranks.FULL_HOUSE;
        } else if (isFlush(cards)) {
            rank = Ranks.FLUSH;
        } else if (isStraight(cards)) {
            rank = Ranks.STRAIGHT;
        } else if (isThreeOfaKind(cards)) {
            rank = Ranks.THREE_OF_KIND;
        } else if (isTwoPair(cards)) {
            rank = Ranks.TWO_PAIR;
        } else if (isOnePair(cards)) {
            rank = Ranks.ONE_PAIR;
        } else {
            rank = Ranks.HIGH_CARD;
        }
        return rank;
    }

    private int findId(String card) {
        int id = -1;
        for (String tmp : orders) {
            id++;
            if (tmp.charAt(0) == card.charAt(0)) break;
        }
        return id;
    }

    public void sortCards(String[] cards) {
        int i;
        int j;
        String temp;
        for (i = 1; i < cards.length; i++) {
            for (j = i; j > 0 && findId(cards[j - 1].charAt(0) + "") < findId(cards[j].charAt(0) + ""); j--) {
                temp = cards[j];
                cards[j] = cards[j - 1];
                cards[j - 1] = temp;
            }
        }
    }

    public boolean sameColors(String[] cards){
        if(cards.length < 5) return false;
        String tmp = cards[0];
        for (String card: cards){
            if (card.charAt(1) != tmp.charAt(1)) {
               return false;
           }
           tmp = card;
        }
        return true;
    }

    public boolean isPoker(String[] cards) {
        sortCards(cards);
        String tmp = cards[0];
        String[] cardsInRow = new String[5];
        int inRow = 0;
        for ( int i = 0; i < cards.length; i++) {
            if (!tmp.equals(cards[i])) {
                int idCurrentCard = findId(cards[i]);
                int idPreviousCard = findId(tmp);
                if (idCurrentCard != idPreviousCard - 1) {
                    if(cards.length - i < 5){
                        return (sameColors(cardsInRow));
                    }
                    else {
                        cardsInRow = new String[5];
                        inRow = 0;
                    }
                } else {
                    cardsInRow[inRow] = tmp;
                    cardsInRow[inRow+1] = cards[i];
                    inRow++;
                }
            }
            tmp = cards[i];
        }
        return sameColors(cardsInRow);
    }

    public boolean isFourOfaKind(String[] cards) {
        sortCards(cards);
        String tmp = cards[0];
        int same = 0;
        int i = 0;
        for (String card : cards) {
            if (tmp.charAt(0) == card.charAt(0)) {
                same++;
            } else if (3 > 1) return (same == 4);
            tmp = card;
            i++;
        }
        return (same == 4);
    }

    public boolean isFullHouse(String[] cards) {
        sortCards(cards);
        String tmp = cards[0];
        int card1 = 0;
        int card2 = 0;
        for (String card : cards) {
            if (card.charAt(0) == tmp.charAt(0)) {
                card2++;
            } else {
                card1 = card2;
                card2 = 1;
            }
            tmp = card;
        }
        return ((card1 == 2) && (card2 == 3)) || ((card2 == 2) && (card1 == 3));
    }

    public boolean isFlush(String[] cards) {
        String tmp = cards[0];
        for (String card : cards) {
            if (card.charAt(1) != tmp.charAt(1)) {
                return false;
            }
            tmp = card;
        }

        return true;
    }

    public boolean isStraight(String[] cards) {
        sortCards(cards);
        String tmp = cards[0];
        int inRow = 1;
        for (String card : cards) {
            if (!tmp.equals(card)) {
                int idCurrentCard = findId(card);
                int idPreviousCard = findId(tmp);
                if (idCurrentCard != idPreviousCard - 1) {
                    return false;
                } else {
                    inRow++;
                }
            }
            tmp = card;
        }
        return (inRow == 5);
    }

    public boolean isThreeOfaKind(String[] cards) {
        sortCards(cards);
        String tmp = "Null";
        int same = 1;
        int i = 0;
        for (String card : cards) {
            if (tmp.charAt(0) == card.charAt(0))
                same++;
            else if (i > 2)
                return (same == 3);
            else
                same = 1;

            tmp = card;
            i++;
        }
        return (same == 3);
    }

    public boolean isTwoPair(String[] cards) {
        sortCards(cards);
        int pairs = 0;
        if(cards.length > 3){
            if (cards[0].charAt(0) == cards[1].charAt(0)) {
                pairs++;
            }
            if (cards[1].charAt(0) == cards[2].charAt(0)) {
                pairs++;
            }
            if (cards[2].charAt(0) == cards[3].charAt(0)) {
                pairs++;
            }
        }
        if (cards.length > 4 && cards[3].charAt(0) == cards[4].charAt(0)) {
            pairs++;
        }
        return (pairs == 2);
    }

    public boolean isOnePair(String[] cards) {
        sortCards(cards);
        String tmp = "Null";
        int same = 0;
        for (String card : cards) {
            if (tmp.charAt(0) == card.charAt(0)) return true;
            tmp = card;
        }
        return false;
    }

}
