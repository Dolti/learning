package com.dolata.patryk;

import java.util.*;

/**
 * Created by Dolti on 2017-01-11.
 */
public class PokerGame {
    private List<String> deck;
    private String[] kinds = {"2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A"};
    private String[] colorKinds = {"S", "H", "D", "C"};
    private String cardsOnTable;
    private int pot;

    PokerGame() {
        deck = new ArrayList<>();
        int pot = 0;
        cardsOnTable = "";
        shuffle();
    }

    private void shuffle() {
        for (int i = 0; i < kinds.length; i++) {
            for (int j = 0; j < colorKinds.length; j++) {
                deck.add(kinds[i] + colorKinds[j]);
                //System.out.println(kinds[i]+colorKinds[j]);
            }
        }
        Collections.shuffle(deck, new Random(System.nanoTime()));
    }

    public void showDeck() {
        for (String card : deck) {
            System.out.print(card + " ");
        }
    }

    public String getCards(int numberOfCards) {
        String result = "";
        if (deck.size() < numberOfCards) {
            return "EOC";
        }
        for (int i = 0; i < numberOfCards; i++) {
            Random rand = new Random(System.nanoTime());
            int id = rand.nextInt(deck.size());
            result += deck.get(id);
            if (i != numberOfCards - 1) result += " ";
            deck.remove(id);
        }
        return result;
    }

    public void takeCoins(Player[] players) {
        System.out.println("Time to bet:");
        Scanner scanner = new Scanner(System.in);
        for (Player player : players) {
            System.out.println(player.getName() + " how many coins you bet?");
            int coins = scanner.nextInt();
            this.pot += player.bet(coins);
        }
    }

    public void showFlop() {
        cardsOnTable += getCards(3);
        System.out.println("Flop = " + cardsOnTable);
    }

    public void showTurn() {
        cardsOnTable += " " + getCards(1);
        System.out.println("Turn = " + cardsOnTable);
    }

    public void showRiver() {
        cardsOnTable +=  " " + getCards(1);
        System.out.println("River = " + cardsOnTable);
    }

    public int getWinner(Player[] players) {
        List<Player> winners = new ArrayList<>();
        int i;
        int j;
        Player temp;

        for (Player player : players) {
            player.getPokerHand().addCards(cardsOnTable);
        }
        for (i = 1; i < players.length; i++) {
            for (j = i; j > 0 && (players[j - 1].getPokerHand().getRank().ordinal() < players[j].getPokerHand().getRank().ordinal()); j--) {
                temp = players[j];
                players[j] = players[j - 1];
                players[j - 1] = temp;
            }
        }
        temp = players[0];
        for (Player player : players) {
            if (temp.getPokerHand().getRank().ordinal() == player.getPokerHand().getRank().ordinal()) {
                winners.add(player);
            }
            temp = player;
        }
            int prize = pot/winners.size();
            System.out.println("Won " + prize + " coins:");
            for (Player player : winners){
                player.setCoins(player.getCoins()+prize);
                System.out.println(player);
            }

        return winners.size();
    }
}
