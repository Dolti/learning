package com.dolti;


import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
public class Main {

    public static void main(String[] args) {
	int[] array = {3,3,5,3};
        System.out.println(stray2(array));
    }
    //2851 - time
    static int stray(int[] numbers) {
        Arrays.sort(numbers);
        int tmp = numbers[0];
        int counter = 0;
        for (int number : numbers){
            if(tmp != number){
                if(counter > 1){
                    tmp =  number;
                }
                break;
            }
            tmp = number;
            counter++;
        }
        return tmp;
    }
    //3244 - time
    static int stray2(int[] numbers){
        HashMap<Integer, Integer> repetitions = new HashMap<>();
        for(int number : numbers){
            if(repetitions.containsKey(number)){
                repetitions.put(number, repetitions.get(number) + 1);
            } else {
                repetitions.put(number, 1);
            }
        }
        for (Map.Entry<Integer, Integer> item : repetitions.entrySet()) {
            if(item.getValue() == 1){
                return item.getKey();
            }
        }
        return -1;
    }
}
