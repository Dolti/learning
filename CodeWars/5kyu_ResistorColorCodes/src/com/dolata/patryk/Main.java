package com.dolata.patryk;

import java.util.HashMap;
import java.util.Map;

/*
0: black, 1: brown, 2: red, 3: orange, 4: yellow, 5: green, 6: blue, 7: violet, 8: gray, 9: white
All resistors have at least three bands, with the first and second bands indicating the first two
digits of the ohms value, and the third indicating the power of ten to multiply them by, for example
a resistor with a value of 47 ohms, which equals 47 * 10^0 ohms, would have the three bands
"yellow violet black".
"10 ohms"        "brown black black gold"
"100 ohms"       "brown black brown gold"
"220 ohms"       "red red brown gold"
"330 ohms"       "orange orange brown gold"
"470 ohms"       "yellow violet brown gold"
"680 ohms"       "blue gray brown gold"
"1k ohms"        "brown black red gold"
"10k ohms"       "brown black orange gold"
"22k ohms"       "red red orange gold"
"47k ohms"       "yellow violet orange gold"
"100k ohms"      "brown black yellow gold"
"330k ohms"      "orange orange yellow gold"
"2M ohms"        "red black green gold"
 */
public class Main {

    public static void main(String[] args) {
        System.out.println(encodeResistorColors("4.7k ohms"));
    }

    public static String encodeResistorColors(String ohmsString) {
        String result = ohmsString.split(" ")[0];
        if (result.matches("\\d+[.]?\\d*")) {
            result = getColors(Integer.parseInt(result));
        } else if (result.matches("\\d+[.]?\\d*k")) {
            result = getColors(Double.parseDouble(result.replace("k", "")) * 1000);
        } else if (result.matches("\\d+[.]?\\d*M")) {
            result = getColors(Double.parseDouble(result.replace("M", "")) * 1000000);
        }

        return result;
    }

    public static int multi(double value){
        int multi = 0;
        while (value % 10 == 0 && value > 99){
            value /= 10;
            multi++;
        }
        return multi;
    }

    public static String getColors(double value){
        Map<Integer, String> vocabulary = initializeVocabulary();
        String result = "";
        double multiple = multi(value);
        value = value/(int)Math.pow(10,multiple);
        String val = ""+ (int)value;
        for (int i = 0; i < val.length(); i++) {
            result += vocabulary.get(Character.getNumericValue(val.charAt(i))) + " ";
        }
        result += vocabulary.get((int)multiple) + " gold";
        return  result;
    }

    public static Map<Integer, String> initializeVocabulary() {
        Map<Integer, String> vocabulary = new HashMap<>();
        vocabulary.put(0, "black");
        vocabulary.put(1, "brown");
        vocabulary.put(2, "red");
        vocabulary.put(3, "orange");
        vocabulary.put(4, "yellow");
        vocabulary.put(5, "green");
        vocabulary.put(6, "blue");
        vocabulary.put(7, "violet");
        vocabulary.put(8, "gray");
        vocabulary.put(9, "white");
        return vocabulary;
    }
}

/*clever from CodeWars
public class EncodeResistorColors {

    private static final String[] CODES = new String[] {"black","brown","red","orange","yellow","green","blue","violet","gray","white"};

    public static String encodeResistorColors(final String os) {
        final String ns = os.split(" ")[0];
        final int factor = ns.endsWith("k") ? 1000 : ns.endsWith("M") ? 1000000 : 1;
        final String vs = "" + (int)(Double.valueOf(ns.replaceAll("[^\\.0-9]","")) * factor);
        return String.format("%s %s %s gold", CODES[Integer.valueOf(vs.substring(0,1))],  CODES[Integer.valueOf(vs.substring(1,2))], CODES[vs.length() - 2]);
    }
}
*/

