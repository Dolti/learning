package com.dolata.patryk;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dolti on 2016-12-29.
 */
public class Solution {
    public int solution(int N, String S) {
        // write your code in Java SE 8
        Map<String, Integer> places = new HashMap<>();

        for (int i = 1; i <= N; i++) {
            for (char r = 'A'; r <= 'K'; r++) {
                String name = i + Character.toString(r);
                places.put(name, 0);
            }
        }

        String[] split = S.split(" ");
        for (String s : split) {
            places.put(s, 1);
        }

        int freeSum = 0;
        int currFree = 0;
        for (int i = 1; i <= N; i++) {
            for (char r = 'A'; r <= 'K'; r++) {
                String name = i + Character.toString(r);
                switch (r) {
                    case 'A':
                        currFree = 0;
                        break;
                    case 'D':
                        currFree = 0;
                        break;
                    case 'H':
                        currFree = 0;
                        break;
                }

                if (places.get(name) == 0) {
                    currFree++;
                } else {
                    if (currFree >= 3) {
                        freeSum++;
                    }
                    currFree = 0;
                }

                if (currFree == 3) {
                    freeSum++;
                }
            }
        }

        return freeSum;
    }
}
