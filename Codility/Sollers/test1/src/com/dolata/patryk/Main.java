package com.dolata.patryk;

public class Main {

    public static void main(String[] args) {
        Solution sol = new Solution();
        int[] array = {-1,2,-3,4,-2,0,-5,-6};
        int[] big = new int[1000];
        for(int i = 0; i < big.length; i++){
            big[i] = -1000;
        }
        big[500] = 2;
        big[501] = 1;
        System.out.println(sol.solution(big));
    }
}
