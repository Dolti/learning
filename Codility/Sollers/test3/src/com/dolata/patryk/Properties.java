package com.dolata.patryk;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Dolti on 2016-12-29.
 */
public class Properties{
    private String name;
    private String town;
    private String ext;
    private Date date;
    private int currNbr;

    public Properties(String data){
        setData(data);
    }
    private void setData(String data){
        //photo.jpg, Warsaw, 2013-09-05 14:08:15
        String[] split = data.split(",");
        this.name = split[0].substring(0,split[0].indexOf('.'));
        this.ext = split[0].substring(split[0].indexOf('.'));
        this.town = split[1];
        this.currNbr = 0;
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            this.date = dt.parse(split[2]);
        } catch (ParseException e){
            e.getStackTrace();
        }
    }

    public void setCurrNbr(int nbr){
        this.currNbr = nbr;
    }

    public int getCurrNbr(){
        return this.currNbr;
    }
    public String getName() {
        return name;
    }

    public String getTown() {
        return town;
    }

    public String getExt() {
        return ext;
    }


    public Date getDate() {
        return date;
    }
}

