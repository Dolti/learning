package com.dolata.patryk;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Patryk Dolata on 2016-12-29.
 */
public class Solution {
    public String solution(String S) {
        // write your code in Java SE 8
        List<Properties> photos = new ArrayList<>();
        String[] split = S.split("\n");
        String result = "";
        for (String s : split) {
            Properties properties = new Properties(s);
            photos.add(properties);
        }
        List<Properties> newPhotos = new ArrayList<>();
        for (Properties p : photos) {
            int maxNumber = coutGroup(p.getTown(), photos);
            String newItem = p.getTown() + changeFormat(p.getCurrNbr(), maxNumber) + p.getExt();
            result += newItem + "\n";
        }
        return result;
    }

    public int coutGroup(String groupName, List<Properties> photos) {
        int value = 0;
        for (Properties p : photos) {
            if (p.getTown().equals(groupName)) {
                p.setCurrNbr(++value);
            }
        }
        return value;
    }

    public String changeFormat(int currNumber, int maxNumber) {
        String formattedNumber = new String();
        int numbers = 1;
        if (maxNumber > 9) {
            while ((maxNumber % 10) >= 0) {
                maxNumber -= numbers * 10;
                numbers++;
                if (maxNumber <= 0) {
                    break;
                }
            }
        }
        String currentNumber = "" + currNumber;
        for (int i = 0; i < numbers - currentNumber.length(); i++) {
            formattedNumber += "0";
        }
        formattedNumber += currNumber;
        return formattedNumber;
    }
}

