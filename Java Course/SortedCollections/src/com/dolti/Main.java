package com.dolti;

import java.util.Map;

public class Main {
    private static StockList stockList = new StockList();

    public static void main(String[] args) {
        StockItem temp = new StockItem("bread", 0.83, 100);
        stockList.addStock(temp);

        temp = new StockItem("cake", 1.20, 7);
        stockList.addStock(temp);

        temp = new StockItem("car", 11.20, 2);
        stockList.addStock(temp);

        temp = new StockItem("chair", 0.20, 200);
        stockList.addStock(temp);

        temp = new StockItem("cup", 1.20, 7);
        stockList.addStock(temp);

        temp = new StockItem("cup", 1.40, 244);
        stockList.addStock(temp);

        temp = new StockItem("door", 5.20, 36);
        stockList.addStock(temp);

        temp = new StockItem("phone", 1.20, 7);
        stockList.addStock(temp);

        temp = new StockItem("juice", 1.20, 241);
        stockList.addStock(temp);

        temp = new StockItem("vase", 9.20, 40);
        stockList.addStock(temp);

        System.out.println(stockList);

        for (String s : stockList.Items().keySet()) {
            System.out.println(s);
        }

        Basket patsBasket = new Basket("Pat");
        sellItem(patsBasket, "car", 1);
        System.out.println(patsBasket);

        sellItem(patsBasket, "car", 1);
        System.out.println(patsBasket);

        sellItem(patsBasket, "car", 1);
        System.out.println(patsBasket);

        sellItem(patsBasket, "spanner", 5);

        sellItem(patsBasket, "juice", 4);
//        System.out.println(patsBasket);

        Basket basket = new Basket("customer");
        sellItem(basket, "cup", 100);
        sellItem(basket, "juice", 5);
        removeItem(basket, "cup", 1);
        System.out.println(basket);

        removeItem(patsBasket, "car", 1);
        removeItem(patsBasket, "cup", 9);
        removeItem(patsBasket, "car", 1);
        System.out.println("cars removed: " + removeItem(patsBasket, "car", 1));
        System.out.println(patsBasket);

        removeItem(patsBasket, "bread", 1);
        removeItem(patsBasket, "cup", 3);
        removeItem(patsBasket, "juice", 4);
        removeItem(patsBasket, "cup", 3);

        System.out.println("\nDisplay stock list before and after checkout");
        System.out.println(basket);
        System.out.println(stockList);
        checkOut(basket);
        System.out.println(basket);
        System.out.println(stockList);

//        temp = new StockItem("pen", 1.12);
//        stockList.Items().put(temp.getName(), temp);
        StockItem car = stockList.Items().get("car");
        if(car != null){
            stockList.Items().get("car").adjustStock(2000);
            stockList.get("car").adjustStock(-1000);
        }
        System.out.println(stockList);
//        for (Map.Entry<String, Double> price : stockList.PriceList().entrySet()) {
//            System.out.println(price.getKey() + " coast " + price.getValue());
//        }
        checkOut(patsBasket);
        System.out.println(patsBasket);
    }

    public static int sellItem(Basket basket, String item, int quantity) {
        StockItem stockItem = stockList.get(item);
        if (stockItem == null) {
            System.out.println("We dont sell " + item);
            return 0;
        }
        if (stockList.reserveStock(item, quantity) != 0) {
            return basket.addToBasket(stockItem, quantity);
        }

        return 0;
    }

    public static int removeItem(Basket basket, String item, int quantity) {
        StockItem stockItem = stockList.get(item);
        if (stockItem == null) {
            System.out.println("We dont sell " + item);
            return 0;
        }
        if (basket.removeFromBasket(stockItem, quantity) == quantity) {
            return stockList.unreserveStock(item, quantity);
        }

        return 0;
    }

    public static void checkOut(Basket basket) {
        for (Map.Entry<StockItem, Integer> item : basket.Items().entrySet()) {
            stockList.sellStock(item.getKey().getName(), item.getValue());
        }
        basket.clearBasket();
    }
}
