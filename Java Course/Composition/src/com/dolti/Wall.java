package com.dolti;

/**
 * Created by Dolti on 2016-11-20.
 */
public class Wall {
    private String direction;

    public Wall(String direction){
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }
}
