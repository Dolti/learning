package com.dolti;

/**
 * Created by Dolti on 2016-11-20.
 */
public class Lamp {
    private String sytle;
    private boolean battery;
    private int globalRating;

    public Lamp(String sytle, boolean battery, int globalRating) {
        this.sytle = sytle;
        this.battery = battery;
        this.globalRating = globalRating;
    }

    public void turnOn(){
        System.out.println("Lamp -> Turning on");
    }
    public String getSytle() {
        return sytle;
    }

    public boolean isBattery() {
        return battery;
    }

    public int getGlobalRating() {
        return globalRating;
    }
}
