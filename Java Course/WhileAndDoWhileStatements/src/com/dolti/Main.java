package com.dolti;

public class Main {

    public static void main(String[] args) {
	    int count = 0;
        while( count != 5 )
        {
            System.out.println("Count value is " + count );
            count++;
        }

        while( true )
        {
            if( count == 5 )
            {
                break;
            }
            System.out.println("Count value is " + count );
            count++;
        }

        for( int i = 1; i < 7; i++ )
        {
            System.out.println("Count value is " + i );
        }

        do {
            System.out.println( "Count value was " + count );
            count++;
        }while( count !=6 );
        int number = 5;
        int finishNumber = 20;
        int totalEvenNuber = 0;
        while( number <= finishNumber && totalEvenNuber < 5 )
        {
            if( !isEvenNumber(number) )
            {
                number++;
                continue;
            }
            totalEvenNuber++;
            System.out.println("Even number " + number );
            number++;
        }
        System.out.println("Total even numbers: " + totalEvenNuber);
    }

    public static boolean isEvenNumber( int n )
    {
        if( n % 2 == 0 )
        {
            return true;
        }
        else
            return false;
    }
}
