package com.dolata.patryk;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by Dolti on 2017-01-22.
 */
@RunWith(Parameterized.class)
public class UtilitiesTestParameterized {
    private Utilities util;
    private String input;
    private String output;

    public UtilitiesTestParameterized(String input, String output) {
        this.input = input;
        this.output = output;
    }

    @org.junit.Before
    public void setup(){
        util = new Utilities();
    }

    @Parameterized.Parameters
    public static Collection<Object> testConditions(){
        return Arrays.asList(new Object[][]{
                {"ABCDEFF", "ABCDEF"},
                {"AB88EFFG", "AB8FG"},
                {"112233445566", "123456"},
                {"ABCDEFF", "ABCDEF"},
                {"A", "A"}
        });
    }

    @org.junit.Test
    public void removePairs() throws Exception{
        assertEquals(output, util.removePairs(input));
    }
}
