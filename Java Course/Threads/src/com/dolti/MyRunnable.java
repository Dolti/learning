package com.dolti;

import static com.dolti.ThreadColor.ANSI_RED;

/**
 * Created by Dolti on 2017-01-05.
 */
public class MyRunnable implements Runnable {
    @Override
    public void run() {
        System.out.println(ANSI_RED + "Hello from MyRunnable class.");
    }
}
