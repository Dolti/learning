package com.dolti;

import static com.dolti.ThreadColor.ANSI_CYAN;
import static com.dolti.ThreadColor.ANSI_PURPLE;
import static com.dolti.ThreadColor.ANSI_RED;

public class Main {

    public static void main(String[] args) {
        System.out.println(ANSI_PURPLE + "Hello from the main thread.");
        Thread anotherThread = new AnotherThread();
        anotherThread.setName("== Another thread ==");
        anotherThread.start();

        new Thread() {
            public void run() {
                System.out.println(ANSI_CYAN + "Hello from the anonymous class thread");
            }
        }.start();
        Thread myRunnableThread = new Thread(new MyRunnable() {
            @Override
            public void run() {
                System.out.println(ANSI_RED + "Hello from the annonymous class's implemetation of run()");
                try{
                    anotherThread.join();
                    System.out.println(ANSI_RED + "AnotherThread terminated, or timed out, so I'm running again.");
                }catch (InterruptedException e){
                    System.out.println(ANSI_RED + "I couldnt wait after all. I was interrupted.");
                }
            }
        });
        myRunnableThread.start();
//        anotherThread.interrupt();
        System.out.println(ANSI_PURPLE + "Hello again from the main thread.");

    }
}
