package com.dolti;

public class Main {

    public static void main(String[] args) {
        // primitives:
        // byte 8 (1 byte)
        // short 16 (2 bytes)
        // int 32 (4 bytes)
        // long 64 (8 bytes)
        // float 32 (4 bytes)
        // double 64 (8 bytes)
        // char 16 (2 bytes)
        // boolean
        String myString = "This is a string";

        myString += ", and more..";
        System.out.println(myString);

        String numberString = "250.55";
        numberString = numberString + "49.95";
        System.out.println("The result is " + numberString );

        String lastString = "10";
        int myInt = 50;
        lastString = lastString + myInt;
        System.out.println("LastString is equal to "+ lastString);
        double doubleNumber = 120.47;
        lastString = lastString + doubleNumber;
        System.out.println("LastString value: " + lastString);

    }
}
