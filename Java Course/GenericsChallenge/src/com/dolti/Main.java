package com.dolti;

public class Main {

    public static void main(String[] args) {
	League<Team<FootballPlayer>> footballLeague = new League<>("AFL");
    Team<FootballPlayer> wymiatacze = new Team<>("Wymiataczne");
    Team<FootballPlayer> takiesobie = new Team<>("Takiesobie");
    Team<FootballPlayer> wPogoniZaSzczesciem = new Team<>("W pogoni za szczesciem");
    Team<FootballPlayer> fcPence = new Team<>("FC Pence");
    Team<BaseballPlayer> baseballTeam = new Team<>("Palka w leb");

    takiesobie.matchResult(wymiatacze, 1, 0);
    takiesobie.matchResult(wPogoniZaSzczesciem, 3,8);
    wPogoniZaSzczesciem.matchResult(wymiatacze, 2, 1);

    footballLeague.add(wymiatacze);
    footballLeague.add(takiesobie);
    footballLeague.add(wPogoniZaSzczesciem);
    footballLeague.add(fcPence);

    footballLeague.showLeagueTable();

    BaseballPlayer pat = new BaseballPlayer("Pat");
    SoccerPlayer beckham = new SoccerPlayer("Beckham");

    Team rawTeam = new Team("Raw Team");
    rawTeam.addPlayer(beckham);
    rawTeam.addPlayer(pat);

    footballLeague.add(rawTeam);

    League<Team> rawLeague = new League<>("Raw");
    rawLeague.add(wymiatacze);
    rawLeague.add(takiesobie);
    rawLeague.add(wPogoniZaSzczesciem);

    League reallyRaw = new League("Really raw");
    reallyRaw.add(wymiatacze);
    reallyRaw.add(takiesobie);
    reallyRaw.add(wPogoniZaSzczesciem);
    }
}
