package com.dolata.patryk;

import java.sql.*;

public class Main {

    public static void main(String[] args) {
	    try(Connection conn = DriverManager.getConnection("jdbc:sqlite:E:\\JAVA\\Kurs JAVA\\repository\\Java Course\\Database\\testjava.db");
            Statement statement = conn.createStatement()){
            conn.setAutoCommit(false);
            statement.execute("CREATE TABLE IF NOT EXISTS contacts " +
                    "(name TEXT, phone INTEGER, email TEXT)");
            statement.execute("INSERT INTO contacts(name, phone, email) " +
                    "VALUES('Joe', 45632, 'joe@anywhere.com')");
           // statement.execute("UPDATE contacts SET phone=78462 WHERE name='Joe'");
           // statement.execute("DELETE FROM contacts WHERE name='Jan'");
//            statement.execute("SELECT * FROM contacts");
//            ResultSet results = statement.getResultSet();
            ResultSet results = statement.executeQuery("SELECT * FROM contacts");
            while (results.next()){
                System.out.println(results.getString("name") + " "
                            + results.getInt("phone") + " " +
                            results.getString("email"));
            }
            statement.close();
            conn.close();
        }catch (SQLException e){
            System.out.println("Something went wrong: " + e.getMessage());
        }
    }
}
