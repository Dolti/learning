package com.dolti;

import java.util.List;

/**
 * Created by Dolti on 2016-12-05.
 */
public interface ISaveable {
    List<String> write();
    void read(List<String> saveValues);

}
