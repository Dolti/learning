package com.dolti;

public class Main {

    public static void main(String[] args) {
        Bank bank = new Bank("National Australia Bank");

        bank.addBranch("Adelaide");
        bank.addCustomer("Adelaide", "Patrick", 50.05);
        bank.addCustomer("Adelaide", "Bob", 175.34);
        bank.addCustomer("Adelaide", "Percy", 220.34);

        bank.addBranch("Sydney");
        bank.addCustomer("Sydney", "Loki", 150.54);

        bank.addCustomerTransaction("Adelaide", "Patrick", 44.22);
        bank.addCustomerTransaction("Adelaide", "Patrick", 14.22);
        bank.addCustomerTransaction("Adelaide", "Patrick", 54.02);

        bank.listCustomers("Adelaide", true);
        bank.listCustomers("Sydney", true);

        bank.addBranch("Melbourne");
        if(!bank.addCustomer("Melbourne", "Brain", 5.42)){
            System.out.println("Error Melbourne branch does not exist");
        }

        if(!bank.addBranch("Adelaide")){
            System.out.println("Adelaide branch already exists");
        }

        if(!bank.addCustomerTransaction("Adelaide", "Fergus", 44.22)){
            System.out.println("Customer does not exist at branch");
        }

        if(!bank.addCustomer("Adelaide", "Patrick", 12.21)){
            System.out.println("Customer Patrick already exists");
        }
    }
}
