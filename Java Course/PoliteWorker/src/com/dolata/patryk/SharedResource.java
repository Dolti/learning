package com.dolata.patryk;

/**
 * Created by Dolti on 2017-01-10.
 */
public class SharedResource {
    private Worker owner;

    public SharedResource(Worker owner) {
        this.owner = owner;
    }

    public synchronized void setOwner(Worker owner) {
        this.owner = owner;
    }

    public Worker getOwner() {
        return owner;
    }
}
