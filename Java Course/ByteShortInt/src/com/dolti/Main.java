package com.dolti;

public class Main {

    public static void main(String[] args) {

        // long has a width of 64
        long myLongValue = 9_223_372_036_854_775_807L;

        // int has a width of 32
        int myMinValue = -2_147_483_648;
	    int myMaxValue = 2_147_483_647;

        // short has a width of 16
        short myShortValue = 32767;

        // byte has a width of 8
        byte myByteValue = 127;


        byte byteValue = 10;
        short shortValue = 20;
        int intValue = 50;
        long longTotal = 50000 + 10L *(byteValue + shortValue + intValue);
        System.out.println("longTotal = " + longTotal);
    }
}
