package com.dolti;

public class Main {

    public static void main(String[] args) {

        Account bobsAccount = new Account();//("123", 50.0,"Bob","lala@wp.pl","231-241-512");

        bobsAccount.setNumber("12345");
        bobsAccount.setBalance(0.00);
        bobsAccount.setCustomerEmailAddress("mymail@wp.pl");
        bobsAccount.setCustomerName("Patrick");
        bobsAccount.setCustomerPhoneNumber("45-212-223-111");

        bobsAccount.withdrawal(100.0);
        bobsAccount.deposit(50.0);
        bobsAccount.withdrawal(100.0);
        bobsAccount.deposit(51.0);
        bobsAccount.withdrawal(100.0);

        Account patsAccount = new Account("Pat", "wp@pk", "213123123");
        System.out.println(patsAccount.getNumber() + " name " + patsAccount.getCustomerName());

        VipCustomer person1 = new VipCustomer();
        System.out.println(person1.getName());

        VipCustomer person2 = new VipCustomer("Bob", 25000.00);
        System.out.println(person2.getName());

        VipCustomer person3 = new VipCustomer("Tim", 200.00, "lol@wp.pl");
        System.out.println(person3.getName());
        System.out.println(person3.getEmail());

    }
}
