package com.dolti;

public class Main {

    public static void main(String[] args) {
        int[][] matrix = {{5,5,5},{2,0,1},{4,2,1}};
        int[][] tmp = buildMatrix(matrix);
        for (int i = 0; i < matrix[0].length; i++) {
            for (int j = 0; j < matrix[1].length; j++) {
                System.out.print(tmp[i][j]);
            }
            System.out.println();
        }
    }

    public static int[][] buildMatrix(int[][] matrix) {
        int[][] result = new int[matrix[0].length][matrix[1].length];
        for (int i = 0; i < matrix[0].length; i++) {
            for (int j = 0; j < matrix[1].length; j++) {
                result[i][j] = matrix[i][j];
            }
        }
        for (int i = 0; i < matrix[0].length; i++) {
            for (int j = 0; j < matrix[1].length; j++) {
                if (matrix[i][j] == 0) {
                    for (int x = 0; x < result[0].length; x++) {
                        for (int y = 0; y < result[1].length; y++) {
                            result[i][y] = 0;
                            result[x][j] = 0;
                        }
                    }
                }
            }
        }
        return result;
    }
}
