package com.dolata.patryk;

public class Main {

    public static void main(String[] args) {
        Node list = new Node(5);
        list.appendToTail(3);
        list.appendToTail(2);
        list.appendToTail(4);
        list.appendToTail(3);
        list.appendToTail(7);
        list.appendToTail(2);
        list.appendToTail(9);
        list.appendToTail(8);
        int i = 6;
        Node newList = partition(list, i);

        while (newList != null) {
            System.out.println(newList.data);
            newList = newList.next;

        }
    }

    public static Node partition(Node node, int x){
        Node beforeStart = null;
        Node afterStart = null;
        while( node != null){
            Node next = node.next;
            if(node.data < x){
                node.next = beforeStart;
                beforeStart = node;
            } else {
                node.next = afterStart;
                afterStart = node;
            }
            node = next;
        }

        Node head = beforeStart;
        while(beforeStart.next != null){
            beforeStart = beforeStart.next;
        }
        beforeStart.next = afterStart;

        return head;
    }
}
