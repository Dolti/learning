package com.dolata.patryk;

/**
 * Created by Dolti on 2017-01-07.
 */
public class Node {
    Node next = null;
    int data = 0;

    public Node(int d){
        data = d;
    }

    public void appendToTail(int data){
        Node end = new Node(data);
        Node n = this;
        while (n.next != null){
            n = n.next;
        }
        n.next = end;
    }
}
