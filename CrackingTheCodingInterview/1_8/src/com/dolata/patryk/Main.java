package com.dolata.patryk;

public class Main {

    public static void main(String[] args) {
        String s1 = "lajkonik";
        String s2 = "niklajko";
        System.out.println(isRotation(s1, s2));
    }

    public static boolean isS2RotationS1(String s1, String s2) {
        if (s1.length() != s2.length() || s1.length() == 0) return false;
        int startIndex = -1, tmp = 0;
        for (int i = 0; i < s2.length(); i++) {
            if (s1.charAt(0) == s2.charAt(i)) {
                startIndex = s2.indexOf(s1.charAt(0));
            }
            if (startIndex != -1) {
                if (s1.charAt(tmp) != s2.charAt(i)) {
                    return false;
                } else {
                    tmp++;
                }
            }
        }
        String sub = s1.substring(0, startIndex);
        return s2.contains(sub);

    }
    //optymalnie
    public static boolean isRotation(String s1, String s2){
        if(s1.length() != s2.length() || s1.length() == 0) return false;
        String s1s1 = s1 + s1;
        return s1s1.contains(s2);
    }
}
